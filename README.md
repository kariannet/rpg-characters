# RPG Characters
## Description
RPG Characters is a C# console application built for hands-on experience with OOP and test-driven development in C#. The application has: 
- Four character classes that has attributes which increase at different rates as the character gains levels, and that can equip different weapons and armour: Mage, Warrior, Ranger and Rogue. These four all inherit from the same abstract class Hero.
- HeroEquipment class to store a character's items. Item classes implemented are Armour and Weapon, which both inherit from abstract class EquipmentItem. Equipped armour alter the total attributes of the character, possibly causing it to deal more damage. Equipped weapons adds to the character's total damage directly.
- Thorough unit testing of the application's functionality.

## Visuals
### Class Diagram
![Class interaction diagram](figures/RPGCharactersClassDiagram.png)

## Installation
Install Visual Studio 2019 with .NET 5.

## Usage
Clone git project repository. Open in Visual Studio. 

## Contributing
[Karianne Tesaker](https://gitlab.com/kariannet)

## Project status
Finished.
