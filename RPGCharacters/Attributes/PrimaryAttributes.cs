﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attributes
{
    /// <summary>
    /// PrimaryAttributes is based on the traditional Three-Stat System for RPG Characters.
    /// 
    /// It specifies three statistics for a character/hero:
    /// Strength - determines the physical strength of the character.
    /// Dexterity – determines the characters ability to attack with speed and nimbleness.
    /// Intelligence – determines the characters affinity with magic.
    /// </summary>
    public class PrimaryAttributes
    {

        private int strength { get; set; }
        private int dexterity { get; set; }
        private int intelligence { get; set; }

        public PrimaryAttributes()
        {
        }

        public PrimaryAttributes(int strength, int dexterity, int intelligence)
        {
            this.strength = strength;
            this.dexterity = dexterity;
            this.intelligence = intelligence;
        }

        /// <summary>
        /// Update this PrimaryAttributes. Replaces the old.
        /// </summary>
        /// <param name="strength">New strength</param>
        /// <param name="dexterity">New dexterity</param>
        /// <param name="intelligence">New intelligence</param>
        public void SetPrimaryAttributes(int strength, int dexterity, int intelligence)
        {
            this.strength = strength;
            this.dexterity = dexterity;
            this.intelligence = intelligence;
        }

        /// <summary>
        /// Returns this strength.
        /// </summary>
        /// <returns>Strength</returns>
        public int Strength()
        {
            return this.strength;
        }

        /// <summary>
        /// Returns this dexterity.
        /// </summary>
        /// <returns>Dexterity</returns>
        public int Dexterity()
        {
            return this.dexterity;
        }

        /// <summary>
        /// Returns this intelligence.
        /// </summary>
        /// <returns>Intelligence</returns>
        public int Intelligence()
        {
            return this.intelligence;
        }

        /// <summary>
        /// Update + operater for this class. Sets how to sum PrimaryAttributes objects.
        /// </summary>
        /// <param name="left">Object for lefthand side of operator</param>
        /// <param name="right">Object for righthand side of operator</param>
        /// <returns>The sum of the two PrimaryAttributes objects</returns>
        public static PrimaryAttributes operator +(PrimaryAttributes left, PrimaryAttributes right)
        {
            return new PrimaryAttributes
            {
                strength = left.strength + right.strength,
                intelligence = left.intelligence + right.intelligence,
                dexterity = left.dexterity + right.dexterity
            };
        }

        /// <summary>
        /// Check for if given object is equal to this.
        /// </summary>
        /// <param name="obj">Object to compare with</param>
        /// <returns>True if equal, false if not.</returns>
        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes attributes &&
                   strength == attributes.strength &&
                   dexterity == attributes.dexterity &&
                   intelligence == attributes.intelligence;
        }

        /// <summary>
        /// Returns hashcode for object.
        /// </summary>
        /// <returns>Hashcode</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(strength, dexterity, intelligence);
        }

    }
}
