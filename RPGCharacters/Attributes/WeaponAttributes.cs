﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attributes
{
    /// <summary>
    /// WeaponAttributes give statistics for Weapons for RPG Characters.
    /// Weapons have three statistics:
    /// Damage - base damage.
    /// Attack speed - attacks per second.
    /// Damage per second (DPS) - calculated by multiplying the two statistics above.
    /// </summary>
    public class WeaponAttributes
    {
        private int damage { get; }
        private double attackSpeed { get; }
        private double dps { get; }

        public WeaponAttributes(int damage, double attackSpeed)
        {
            this.damage = damage;
            this.attackSpeed = attackSpeed;
            this.dps = (double)damage * attackSpeed;
        } 

        /// <summary>
        /// Returns this weapon's damage per second (DPS).
        /// </summary>
        /// <returns>DPS</returns>
        public double DPS()
        {
            return this.dps;
        }

        /// <summary>
        /// Check for if given object is equal to this.
        /// </summary>
        /// <param name="obj">Object to compare with</param>
        /// <returns>True if equal, false if not.</returns>
        public override bool Equals(object obj)
        {
            return obj is WeaponAttributes attributes &&
                   damage == attributes.damage &&
                   attackSpeed == attributes.attackSpeed &&
                   dps == attributes.dps;
        }

        /// <summary>
        /// Returns hashcode for object.
        /// </summary>
        /// <returns>Hashcode</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(damage, attackSpeed, dps);
        }
    }
}
