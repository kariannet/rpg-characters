﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.CustomExceptions
{
    /// <summary>
    /// Custom error for errors related to characters/heros equipping armour.
    /// </summary>
    public class InvalidArmourException : Exception
    {
        /// <summary>
        /// Constructor where you can specify the error message.
        /// </summary>
        /// <param name="message">error message</param>
        public InvalidArmourException(string message) : base(message)
        {
        }

        /// <summary>
        /// Default error message.
        /// </summary>
        public override string Message => "Invalid armour for this hero.";
    }
}
