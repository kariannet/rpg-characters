﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.CustomExceptions
{
    /// <summary>
    /// Custom error for errors related to characters/heros equipping weapons.
    /// </summary>
    public class InvalidWeaponException : Exception
    {
        /// <summary>
        /// Constructor where you can specify the error message.
        /// </summary>
        /// <param name="message">error message</param>
        public InvalidWeaponException(string message) : base(message)
        {
        }

        /// <summary>
        /// Default error message.
        /// </summary>
        public override string Message => "Invalid weapon for this hero.";
    }
}
