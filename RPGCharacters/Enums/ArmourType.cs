﻿public enum ArmourType
{
    ARMOUR_CLOTH,
    ARMOUR_LEATHER,
    ARMOUR_MAIL,
    ARMOUR_PLATE,
}