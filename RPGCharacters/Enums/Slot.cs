﻿public enum Slot
{
    SLOT_HEAD,
    SLOT_BODY,
    SLOT_LEGS,
    SLOT_WEAPON,
}