﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacters.EquipmentItems;

namespace RPGCharacters.Equipment
{
    /// <summary>
    /// Stores the equipment (armour and weapon) for RPG Characters. 
    /// Equipment has four slots to store items (1 per slot):
    /// - Head
    /// - Body
    /// - Legs
    /// - Weapon.
    /// These are specified using enumerator Slot.
    /// </summary>
    public class HeroEquipment
    {

        private Dictionary<Slot, EquipmentItem> equipment { get; }

        public HeroEquipment()
        {
            equipment = new Dictionary<Slot, EquipmentItem>(4);
            for (int indexSlot = 0; indexSlot < 4; indexSlot++) equipment[(Slot)indexSlot] = null;
            
        }

        /// <summary>
        /// Returns this equipment dictionary.
        /// </summary>
        /// <returns>Equipment</returns>
        public Dictionary<Slot, EquipmentItem> Equipment()
        {
            return this.equipment;
        }

        /// <summary>
        /// Sets the value of the given slot to the given item in the equipment dictionary.
        /// </summary>
        /// <param name="slot">The dictionary key</param>
        /// <param name="item">The set value</param>
        public void SetItem(Slot slot, EquipmentItem item)
        {
            equipment[slot] = item;
        }


        /// <summary>
        /// Get the item equiped at the given slot of the equipment dictionary.
        /// </summary>
        /// <param name="slot">The dictionary key</param>
        /// <returns>Item stored at the given slot.</returns>
        public EquipmentItem GetItem(Slot slot)
        {
            return equipment[slot];
        }


    }
}
