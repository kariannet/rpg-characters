﻿using RPGCharacters.Attributes;
using RPGCharacters.CustomExceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.EquipmentItems
{
    /// <summary>
    /// Armour is a type of item for RPG Characters. It is a child class of EquipmentItem.
    /// It can be equipped in slots head, body and legs, but not in the weapon slot. 
    /// Armours have two attributes in addition to the ones in EquipmentItem:
    /// - Armour Type: what type of armour this is. There are four: cloth, leather, mail and 
    /// plate, specified by the enumerator ArmourType.
    /// - Armour attributes: specifications to how much this armour will increase the total 
    /// attributes of hero who equips it.
    /// </summary>
    public class Armour : EquipmentItem
    {
        private ArmourType armourType { get; }
        private PrimaryAttributes armourAttributes { get; }

        /// <summary>
        /// Class constructor. Throws InvalidArmourException if trying to set itemSlot 
        /// to Slot.Slot_Weapon.
        /// </summary>
        /// <param name="itemName">Armour name</param>
        /// <param name="itemLevel">Level required to equip armour</param>
        /// <param name="itemSlot">Slot to store armour when equipped</param>
        /// <param name="armourType">Type of armour</param>
        /// <param name="armourAttributes">Primary attributes for armour</param>
        public Armour(string itemName, int itemLevel, Slot itemSlot, 
            ArmourType armourType, PrimaryAttributes armourAttributes) 
            : base(itemName, itemLevel, itemSlot)
        {
            if (itemSlot == Slot.SLOT_WEAPON)
            {
                throw new InvalidArmourException("Armour can't be equipped in weapon slot.");
            }
            this.armourType = armourType;
            this.armourAttributes = armourAttributes;
        }

        /// <summary>
        /// Return this Armour's type.
        /// </summary>
        /// <returns>ArmourType</returns>
        public ArmourType ArmourType()
        {
            return this.armourType;
        }

        /// <summary>
        /// Return this armour's attributes.
        /// </summary>
        /// <returns>ArmourAttributes</returns>
        public PrimaryAttributes ArmourAttributes()
        {
            return this.armourAttributes;
        }
    }
}
