﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.EquipmentItems
{
    /// <summary>
    /// Abstract class EquipmentItem describe all methods and parameters that are common
    /// for all RPG Character items (weapons and armours). There are three common attributes:
    /// Item name - name of the item,
    /// Item level - the required character level topp equip this item
    /// Item slot - the slot in which this item can be equipped.
    /// </summary>
    public abstract class EquipmentItem
    {
        protected string itemName { get; }
        protected int itemLevel { get; }
        protected Slot itemSlot { get; }

        protected EquipmentItem(string itemName, int itemLevel, Slot itemSlot)
        {
            this.itemName = itemName;
            this.itemLevel = itemLevel;
            this.itemSlot = itemSlot;
        }

        /// <summary>
        /// Returns the required level for equipping this item.
        /// </summary>
        /// <returns>ItemLevel</returns>
        public int ItemLevel()
        {
            return this.itemLevel;
        }

        /// <summary>
        /// Returns the item name.
        /// </summary>
        /// <returns>ItemName</returns>
        public string ItemName()
        {
            return this.itemName;
        }

        /// <summary>
        /// Returns the slot where this item should be equipped.
        /// </summary>
        /// <returns>ItemSlot</returns>
        public Slot ItemSlot()
        {
            return this.itemSlot;
        }


    }
}
