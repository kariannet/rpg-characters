﻿using RPGCharacters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.EquipmentItems
{
    /// <summary>
    /// Weapon is a type of item for RPG Characters. It is a child class of EquipmentItem.
    /// It will only be equipped in the weapon slot. 
    /// Weapons have two attributes in addition to the ones in EquipmentItem:
    /// - Weapon type: what type of weapon this is. There are seven: ax, bow, dagger, hammer,
    /// staff, sword and wand, specified by the enumerator WeaponType.
    /// - Weapon attributes: specifications that will increase the total damage of the hero 
    /// who equips it.
    /// </summary>
    public class Weapon : EquipmentItem
    {
        private WeaponType weaponType { get; }
        private WeaponAttributes weaponAttributes { get; }

        public Weapon(string itemName, int itemLevel, Slot itemSlot, WeaponType weaponType, WeaponAttributes weaponAttributes) : base(itemName, itemLevel, itemSlot)
        {
            this.weaponType = weaponType;
            this.weaponAttributes = weaponAttributes;
        }

        /// <summary>
        /// Returns this WeaponAttributes.
        /// </summary>
        /// <returns>WeaponAttributes</returns>
        public WeaponAttributes WeaponAttributes()
        {
            return this.weaponAttributes;
        }

        /// <summary>
        /// Returns this weapon's type.
        /// </summary>
        /// <returns>WeaponType</returns>
        public WeaponType WeaponType()
        {
            return this.weaponType;
        }

    }
}
