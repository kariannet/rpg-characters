﻿using RPGCharacters.Attributes;
using RPGCharacters.CustomExceptions;
using RPGCharacters.Equipment;
using RPGCharacters.EquipmentItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Hero
{
    /// <summary>
    /// Abstract class that implements the IHero interface. Heros have five attributes:
    /// Character name - name of character.
    /// Character level - current level of character.
    /// Equipment - this character's equipment (weapon and armour).
    /// Base primary attributes - the character's base attributes, decided by character class and level.
    /// Total primary attributes - the total primary attributes, which are the sum of the hero's base
    /// primary attributes and their equipped armour's attributes.
    /// </summary>
    public abstract class Hero : IHero
    {
        protected string name { get; }
        protected int level { get; set; }
        protected HeroEquipment equipment { get; set; }
        protected PrimaryAttributes basePrimaryAttributes { get; set; }
        protected PrimaryAttributes totalPrimaryAttributes { get; set; }

        public Hero(string name)
        {
            this.name = name;
            // start at level 1:
            this.level = 1;
            this.equipment = new HeroEquipment();
        }

        #region Public methods

        public int Level()
        {
            return this.level;
        }

        public string Name()
        {
            return this.name;
        }

        public HeroEquipment Equipment()
        {
            return this.equipment;
        }

        public PrimaryAttributes BasePrimaryAttributes()
        {
            return this.basePrimaryAttributes;
        }

        public PrimaryAttributes TotalPrimaryAttributes()
        {
            return this.totalPrimaryAttributes;
        }

        public string StatsDisplay()
        {
            StringBuilder allStats = new StringBuilder();

            allStats.AppendLine("Character name: " + this.name);
            allStats.AppendLine("Character level: " + this.level);
            allStats.AppendLine("Strength: " + this.totalPrimaryAttributes.Strength());
            allStats.AppendLine("Dexterity: " + this.totalPrimaryAttributes.Dexterity());
            allStats.AppendLine("Intelligence: " + this.totalPrimaryAttributes.Intelligence());
            allStats.AppendLine("Damage: " + this.TotalDamage());

            return allStats.ToString();
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Actions to be performed by Hero upon leveling up. The level increases by 1, the 
        /// base primary attributes are updated according to the hero class, and the total
        /// primary attributes are then updated.
        /// </summary>
        /// <param name="strengthIncrement">How much strength should increase for the hero</param>
        /// <param name="dexterityIncrement">How much dexterity should increase for the hero</param>
        /// <param name="intelligenceIncrement">How much intelligence should increase for the hero</param>
        protected void LevelUpActions(int strengthIncrement, int dexterityIncrement, int intelligenceIncrement)
        {
            this.level++;
            this.basePrimaryAttributes.SetPrimaryAttributes(
                this.basePrimaryAttributes.Strength() + strengthIncrement,
                this.basePrimaryAttributes.Dexterity() + dexterityIncrement,
                this.basePrimaryAttributes.Intelligence() + intelligenceIncrement
                );
            this.UpdateTotalPrimaryAttributes();
        }

        /// <summary>
        /// Actions to be performed upon trying to equip a weapon. Throws InvalidWeaponException if this hero
        /// has too low level to equip this weapon or if this hero is not allowed to equip this type of weapon.
        /// </summary>
        /// <param name="forbiddenWeaponTypes">Weapon types this hero is not allowed to equip.</param>
        /// <param name="weapon">The weapon the hero is trying to equip</param>
        /// <returns>Success meassage "New weapon equipped!" if successful.</returns>
        protected string EquipWeaponActions(WeaponType[] forbiddenWeaponTypes, Weapon weapon)
        {
            // Throw errors, if needed:
            if (weapon.ItemLevel() > this.level)
            {
                throw new InvalidWeaponException("This Hero's level is too low to equip this weapon.");
            }
            if (Array.Exists(forbiddenWeaponTypes , weaponType => weaponType == weapon.WeaponType()))
            {
                throw new InvalidWeaponException("This Hero can not equip this type of weapon.");
            }
            // No errors, equip item:
            this.equipment.SetItem(Slot.SLOT_WEAPON, weapon);

            return "New weapon equipped!";
        }

        /// <summary>
        /// Actions to be performed upon trying to equip an armour. Throws InvalidArmourException if this hero
        /// has too low level to equip this armour or if this hero is not allowed to equip this type of armour.
        /// </summary>
        /// <param name="forbiddenArmourTypes">Armour types this hero is not allowed to equip.</param>
        /// <param name="armour">The armour the hero is trying to equip.</param>
        /// <returns>Success meassage "New armour equipped!" if successful.</returns>
        protected string EquipArmourActions(ArmourType[] forbiddenArmourTypes, Armour armour)
        {
            // Throw errors, if needed:
            if (armour.ItemLevel() > this.level)
            {
                throw new InvalidArmourException("This Hero's level is too low to equip this armour.");
            }
            if (Array.Exists(forbiddenArmourTypes, armourType => armourType == armour.ArmourType()))
            {
                throw new InvalidArmourException("This Hero can not equip this type of armour.");
            }
            // No errors, equip item:
            this.equipment.SetItem(armour.ItemSlot(), armour);
            // Update total attributes:
            this.UpdateTotalPrimaryAttributes();

            return "New armour equipped!";
        }

        /// <summary>
        /// Updating the hero's total primary attributes. To be called when changes have been made to the 
        /// hero that should affect the total primary attributes.
        /// </summary>
        protected void UpdateTotalPrimaryAttributes()
        {
            this.totalPrimaryAttributes = new PrimaryAttributes(0, 0, 0);
            this.totalPrimaryAttributes += this.basePrimaryAttributes;
            foreach (KeyValuePair<Slot, EquipmentItem> item in this.equipment.Equipment())
            {
                if (item.Value != null && item.Value.GetType() == typeof(Armour))
                {
                    Armour armour = (Armour)item.Value;
                    this.totalPrimaryAttributes += armour.ArmourAttributes();
                }
            }
        }

        /// <summary>
        /// Calculation of the hero's total damage.
        /// </summary>
        /// <param name="damagingPrimaryAttribute">The total primary attribute that affects the hero's 
        /// total damage. </param>
        /// <returns>Total damage.</returns>
        protected double TotalDamageActions(int damagingPrimaryAttribute)
        {
            if (this.equipment.GetItem(Slot.SLOT_WEAPON) != null && 
                this.equipment.GetItem(Slot.SLOT_WEAPON).GetType() == typeof(Weapon))
            {
                Weapon weapon = (Weapon)this.equipment.GetItem(Slot.SLOT_WEAPON);
                return weapon.WeaponAttributes().DPS() * (1.0 + ((double)damagingPrimaryAttribute / 100.0));
            }
            return 1.0 + ((double)damagingPrimaryAttribute / 100.0);
        }

        #endregion

        #region Abstract Methods
        public abstract string EquipArmour(Armour armour);
        public abstract string EquipWeapon(Weapon weapon);
        public abstract void LevelUp();
        public abstract double TotalDamage();

        #endregion

    }
}
