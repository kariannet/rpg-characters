﻿using RPGCharacters.Attributes;
using RPGCharacters.Equipment;
using RPGCharacters.EquipmentItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Hero
{
    /// <summary>
    /// The IHero interface specifies the methods required for the RPG character classes.
    /// </summary>
    public interface IHero
    {

        /// <summary>
        /// Returns the total damage this Hero can deal.
        /// </summary>
        /// <returns>Total damage</returns>
        double TotalDamage();

        /// <summary>
        /// Returns this Hero's level.
        /// </summary>
        /// <returns>Level</returns>
        int Level();

        /// <summary>
        /// Returns this Hero's name.
        /// </summary>
        /// <returns>Name</returns>
        string Name();

        /// <summary>
        /// Returns this Hero's equipment (armour and weapon).
        /// </summary>
        /// <returns>Equipment</returns>
        HeroEquipment Equipment();

        /// <summary>
        /// Returns this Hero's base primary attributes (strength, dexterity, intelligence).
        /// </summary>
        /// <returns>Base attributes</returns>
        PrimaryAttributes BasePrimaryAttributes();

        /// <summary>
        /// Returns this Hero's total primary attributes (strength, dexterity, intelligence).
        /// Which is the sum of their armour's primary attributes and their base primary attributes.
        /// </summary>
        /// <returns>Total primary attributes</returns>
        PrimaryAttributes TotalPrimaryAttributes();

        /// <summary>
        /// Performs the necessary updates to the Hero attributes when leveling up.
        /// </summary>
        void LevelUp();

        /// <summary>
        /// Equips the Hero with the given Armour object at its allocated slot. Throws 
        /// InvalidArmourexception if the armour is the wrong type for the Hero or if it 
        /// requires a higher level than the Hero has. Upon successful equipment, the Hero's
        /// total primary attributes will be updated with the armour primary attributes boost.
        /// </summary>
        /// <param name="armour">Armour to equip hero with</param>
        /// <returns>Success meassage "New armour equipped!" if successful.</returns>
        string EquipArmour(Armour armour);

        /// <summary>
        /// Equips the Hero with the given Weapon object in the weapon slot. Throws 
        /// InvalidWeaponException if the weapon is the wrong type for the hero, or if it 
        /// requires a higher level than the hero has. Upon successful equipment, the Hero's
        /// total damage will be updated with the weapon DPS.
        /// </summary>
        /// <param name="weapon">Weapon to equip hero with</param>
        /// <returns>Success meassage "New weapon equipped!" if successful.</returns>
        string EquipWeapon(Weapon weapon);

        /// <summary>
        /// Display the hero's statistics to a character sheet.
        /// </summary>
        /// <returns>StringBuilder with all hero statistics</returns>
        string StatsDisplay();

    }
}
