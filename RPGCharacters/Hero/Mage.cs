﻿using RPGCharacters.Attributes;
using RPGCharacters.CustomExceptions;
using RPGCharacters.Equipment;
using RPGCharacters.EquipmentItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Hero
{
    /// <summary>
    /// The Mage class is a child class of Hero. Mages start with:
    /// Strength = 1, dexterity = 1 and intelligence = 8,
    /// and increases the attributes with:
    /// Strength = 1, dexterity = 1 and intelligence = 5,
    /// upon levelling up.
    /// Mages can only equip armour of type cloth, and weapon of types staff and wand.
    /// </summary>
    public class Mage : Hero
    {
        public Mage(string name) : base(name)
        {
            this.basePrimaryAttributes = new PrimaryAttributes(1, 1, 8);
            this.UpdateTotalPrimaryAttributes();
        }

        public override string EquipArmour(Armour armour)
        {
            return this.EquipArmourActions(new ArmourType[] 
            { 
                ArmourType.ARMOUR_LEATHER, 
                ArmourType.ARMOUR_MAIL, 
                ArmourType.ARMOUR_PLATE 
            }, armour);
        }

        public override string EquipWeapon(Weapon weapon)
        {
            return this.EquipWeaponActions(new WeaponType[] 
            {
                WeaponType.WEAPON_AXE,
                WeaponType.WEAPON_BOW,
                WeaponType.WEAPON_DAGGER,
                WeaponType.WEAPON_HAMMER,
                WeaponType.WEAPON_SWORD
            }, weapon);
        }

        public override void LevelUp()
        {
            this.LevelUpActions(1, 1, 5);
        }

        public override double TotalDamage()
        {
            Console.WriteLine("input: " + this.totalPrimaryAttributes.Intelligence());
            return this.TotalDamageActions(this.totalPrimaryAttributes.Intelligence());
        }
    }
}
