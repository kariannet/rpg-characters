﻿using RPGCharacters.Attributes;
using RPGCharacters.EquipmentItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Hero
{
    /// <summary>
    /// The Ranger class is a child class of Hero. Rangers start with:
    /// Strength = 1, dexterity = 7 and intelligence = 1,
    /// and increases the attributes with:
    /// strength = 1, dexterity = 5 and intelligence = 1,
    /// upon levelling up.
    /// Rangers can only equip armour of types leather and mail, and weapon of type bow.
    /// </summary>
    public class Ranger : Hero
    {
        public Ranger(string name) : base(name)
        {
            this.basePrimaryAttributes = new PrimaryAttributes(1, 7, 1);
            this.UpdateTotalPrimaryAttributes();
        }

        public override string EquipArmour(Armour armour)
        {
            return this.EquipArmourActions(new ArmourType[]
            {
                ArmourType.ARMOUR_CLOTH,
                ArmourType.ARMOUR_PLATE
            }, armour);
        }

        public override string EquipWeapon(Weapon weapon)
        {
            return this.EquipWeaponActions(new WeaponType[] 
            { 
                WeaponType.WEAPON_AXE,
                WeaponType.WEAPON_DAGGER,
                WeaponType.WEAPON_HAMMER,
                WeaponType.WEAPON_STAFF,
                WeaponType.WEAPON_SWORD,
                WeaponType.WEAPON_WAND
            }, weapon);
        }

        public override void LevelUp()
        {
            this.LevelUpActions(1, 5, 1);
        }

        public override double TotalDamage()
        {
            return this.TotalDamageActions(this.totalPrimaryAttributes.Dexterity());
        }

    }
}
