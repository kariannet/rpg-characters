﻿using RPGCharacters.Attributes;
using RPGCharacters.EquipmentItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Hero
{
    /// <summary>
    /// The Rogue class is a child class of Hero. Rogues start with:
    /// Strength = 2, dexterity = 6 and intelligence = 1,
    /// and increases the attributes with:
    /// strength = 1, dexterity = 4 and intelligence = 1,
    /// upon levelling up.
    /// Rogues can only equip armour of types leather and mail, and weapons of type dagger and sword.
    /// </summary>
    public class Rogue : Hero
    {
        public Rogue(string name) : base(name)
        {
            this.basePrimaryAttributes = new PrimaryAttributes(2, 6, 1);
            this.UpdateTotalPrimaryAttributes();
        }

        public override string EquipArmour(Armour armour)
        {
            return this.EquipArmourActions(new ArmourType[]
            {
                ArmourType.ARMOUR_CLOTH,
                ArmourType.ARMOUR_PLATE
            }, armour);
        }

        public override string EquipWeapon(Weapon weapon)
        {
            return this.EquipWeaponActions(new WeaponType[]
            {
                WeaponType.WEAPON_AXE,
                WeaponType.WEAPON_BOW,
                WeaponType.WEAPON_HAMMER,
                WeaponType.WEAPON_STAFF,
                WeaponType.WEAPON_WAND
            }, weapon);
        }

        public override void LevelUp()
        {
            this.LevelUpActions(1, 4, 1);
        }

        public override double TotalDamage()
        {
            return this.TotalDamageActions(this.totalPrimaryAttributes.Dexterity());
        }
    }
}
