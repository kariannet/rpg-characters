﻿using RPGCharacters.Attributes;
using RPGCharacters.EquipmentItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Hero
{
    /// <summary>
    /// The Warrior class is a child class of Hero. Warriors start with:
    /// Strength = 5, dexterity = 2 and intelligence = 1,
    /// and increases the attributes with:
    /// strength = 3, dexterity = 2 and intelligence = 1,
    /// upon levelling up.
    /// Warriors can only equip armour of types plate and mail, and weapon of types ax, hammer and sword.
    /// </summary>
    public class Warrior : Hero
    {
        public Warrior(string name) : base(name)
        {
            this.basePrimaryAttributes = new PrimaryAttributes(5, 2, 1);
            this.UpdateTotalPrimaryAttributes();
        }

        public override string EquipArmour(Armour armour)
        {
            return this.EquipArmourActions(new ArmourType[]
            {
                ArmourType.ARMOUR_LEATHER,
                ArmourType.ARMOUR_CLOTH
            }, armour);
        }

        public override string EquipWeapon(Weapon weapon)
        {
            return this.EquipWeaponActions(new WeaponType[]
            {
                WeaponType.WEAPON_BOW,
                WeaponType.WEAPON_DAGGER,
                WeaponType.WEAPON_STAFF,
                WeaponType.WEAPON_WAND
            }, weapon);
        }

        public override void LevelUp()
        {
            this.LevelUpActions(3, 2, 1);
        }

        public override double TotalDamage()
        {
            return this.TotalDamageActions(this.totalPrimaryAttributes.Strength());
        }

    }
}
