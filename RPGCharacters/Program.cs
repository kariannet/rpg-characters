﻿using RPGCharacters.Attributes;
using RPGCharacters.EquipmentItems;
using RPGCharacters.Hero;
using System;

namespace RPGCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Mage mage = new Mage("Anita");
            Console.WriteLine(mage.StatsDisplay());
            mage.LevelUp();
            Console.WriteLine(mage.StatsDisplay());

            Warrior warrior = new Warrior("Olaug-Helena");
            Console.WriteLine(warrior.StatsDisplay());
            warrior.LevelUp();
            Console.WriteLine(warrior.StatsDisplay());
            warrior.EquipArmour(new Armour("Plate", 1, Slot.SLOT_BODY, ArmourType.ARMOUR_PLATE, new PrimaryAttributes(1,2,0)));
            Console.WriteLine(warrior.StatsDisplay());
            warrior.EquipWeapon(new Weapon("Mjølnir", 2, Slot.SLOT_WEAPON, WeaponType.WEAPON_HAMMER, new WeaponAttributes(4, 1.6)));
            Console.WriteLine(warrior.StatsDisplay());
        }
    }
}
