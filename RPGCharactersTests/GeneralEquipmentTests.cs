﻿using RPGCharacters.Attributes;
using RPGCharacters.Equipment;
using RPGCharacters.EquipmentItems;
using System;
using Xunit;

namespace RPGCharactersTests
{
    public class GeneralEquipmentTests
    {
        #region Basic Tests of item constructors, attributes and equipment
        [Fact]
        public void GetWeaponDPS_ValidateWeaponDPS_ShouldCreateWeaponWithCorrectDPS()
        {
            // Arrange
            Weapon testAxe = new Weapon
            (
                "Common axe",
                1,
                Slot.SLOT_WEAPON,
                WeaponType.WEAPON_AXE,
                new WeaponAttributes(7, 1.1)
            );
            double expected = 7*1.1;
            // Act
            double actual = testAxe.WeaponAttributes().DPS();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void HeroEquipmentConstructor_Initialize_ShouldCreateWithCorrectSize()
        {
            // Arrange
            HeroEquipment equipment = new HeroEquipment();
            int expected = 4;
            // Act
            int actual = equipment.Equipment().Count;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(Slot.SLOT_BODY)]
        [InlineData(Slot.SLOT_HEAD)]
        [InlineData(Slot.SLOT_LEGS)]
        [InlineData(Slot.SLOT_WEAPON)]
        public void HeroEquipmentConstructor_Initialize_ShouldCreateWithCorrectSlots(Slot slot)
        {
            // Arrange
            HeroEquipment equipment = new HeroEquipment();
            // Act
            bool actual = equipment.Equipment().ContainsKey(slot);
            // Assert
            Assert.True(actual);
        }

        [Fact]
        public void HeroEquipmentSetItem_SetWeaponItem_ShouldReturnWeapon()
        {
            // Arrange
            HeroEquipment equipment = new HeroEquipment();
            Weapon testBow = new Weapon
            (
                "Common bow",
                1,
                Slot.SLOT_WEAPON,
                WeaponType.WEAPON_BOW,
                new WeaponAttributes(12, 0.8)
            );
            // Act
            equipment.SetItem(testBow.ItemSlot(), testBow);
            // Assert
            Assert.Equal(testBow, equipment.GetItem(testBow.ItemSlot()));
        }
        #endregion
    }
}
