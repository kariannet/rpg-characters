﻿using RPGCharacters.Attributes;
using RPGCharacters.Equipment;
using RPGCharacters.EquipmentItems;
using RPGCharacters.Hero;
using System;
using Xunit;

namespace RPGCharactersTests
{
    public class HeroTests
    {
        #region HeroTests Initialization and level up
        [Fact]
        public void Constructor_Initialize_ShouldCreateWithLevel1()
        {
            // Arrange
            Mage mage = new Mage("Alice");
            int expected = 1;
            // Act
            int actual = mage.Level();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_GainingOneLevel_ShouldReturnLevel2()
        {
            // Arrange
            Mage mage = new Mage("Molly");
            mage.LevelUp();
            int expectedLevel = 2;
            // Act
            int actualLevel = mage.Level();
            // Assert
            Assert.Equal(expectedLevel, actualLevel);
        }

        [Fact]
        public void MageConstructor_Initialize_ShouldCreateWithCorrectAttributes()
        {
            // Arrange
            Mage mage = new Mage("Belinda");
            PrimaryAttributes expected = new PrimaryAttributes(1, 1, 8);
            // Act
            PrimaryAttributes actual = mage.BasePrimaryAttributes();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorConstructor_Initialize_ShouldCreateWithCorrectAttributes()
        {
            // Arrange
            Warrior warrior = new Warrior("Mathilda");
            PrimaryAttributes expected = new PrimaryAttributes(5, 2, 1);
            // Act
            PrimaryAttributes actual = warrior.BasePrimaryAttributes();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RogueConstructor_Initialize_ShouldCreateWithCorrectAttributes()
        {
            // Arrange
            Rogue rogue = new Rogue("Rouge");
            PrimaryAttributes expected = new PrimaryAttributes(2, 6, 1);
            // Act
            PrimaryAttributes actual = rogue.BasePrimaryAttributes();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RangerConstructor_Initialize_ShouldCreateWithCorrectAttributes()
        {
            // Arrange
            Ranger elf = new Ranger("Legolas");
            PrimaryAttributes expected = new PrimaryAttributes(1, 7, 1);
            // Act
            PrimaryAttributes actual = elf.BasePrimaryAttributes();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MageLevelUp_GainingOneLevel_ShouldReturnUpdatedAttributes()
        {
            // Arrange
            Mage mage = new Mage("Sandra");
            mage.LevelUp();
            PrimaryAttributes expectedBaseAttributes = new PrimaryAttributes(2, 2, 13);
            // Act
            PrimaryAttributes actualBaseAttributes = mage.BasePrimaryAttributes();
            // Assert
            Assert.Equal(expectedBaseAttributes, actualBaseAttributes);
        }

        [Fact]
        public void WarriorLevelUp_GainingOneLevel_ShouldReturnUpdatedAttributes()
        {
            // Arrange
            Warrior warrior = new Warrior("Wendy");
            warrior.LevelUp();
            PrimaryAttributes expectedBaseAttributes = new PrimaryAttributes(8, 4, 2);
            // Act
            PrimaryAttributes actualBaseAttributes = warrior.BasePrimaryAttributes();
            // Assert
            Assert.Equal(expectedBaseAttributes, actualBaseAttributes);
        }

        [Fact]
        public void RogueLevelUp_GainingOneLevel_ShouldReturnUpdatedAttributes()
        {
            // Arrange
            Rogue rogue = new Rogue("Emilia Nighthaven");
            rogue.LevelUp();
            PrimaryAttributes expectedBaseAttributes = new PrimaryAttributes(3, 10, 2);
            // Act
            PrimaryAttributes actualBaseAttributes = rogue.BasePrimaryAttributes();
            // Assert
            Assert.Equal(expectedBaseAttributes, actualBaseAttributes);
        }

        [Fact]
        public void RangerLevelUp_GainingOneLevel_ShouldReturnUpdatedAttributes()
        {
            // Arrange
            Ranger ranger = new Ranger("Silvia");
            ranger.LevelUp();
            PrimaryAttributes expectedBaseAttributes = new PrimaryAttributes(2, 12, 2);
            // Act
            PrimaryAttributes actualBaseAttributes = ranger.BasePrimaryAttributes();
            // Assert
            Assert.Equal(expectedBaseAttributes, actualBaseAttributes);
        }
        #endregion


    }
}
