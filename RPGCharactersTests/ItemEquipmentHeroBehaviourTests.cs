﻿using RPGCharacters.Attributes;
using RPGCharacters.CustomExceptions;
using RPGCharacters.Equipment;
using RPGCharacters.EquipmentItems;
using RPGCharacters.Hero;
using System;
using Xunit;

namespace RPGCharactersTests
{
    public class ItemEquipmentHeroBehaviourTests
    {
        #region Hero and items for testing
        Warrior warrior = new Warrior("Kjell");

        Armour testPlateBody = new Armour
        (
            "Common plate body armor",
            1,
            Slot.SLOT_BODY,
            ArmourType.ARMOUR_PLATE,
            new PrimaryAttributes(1, 0, 0)
        );


        Armour testClothHead = new Armour
        (
            "Common cloth head armor",
            1,
            Slot.SLOT_HEAD,
            ArmourType.ARMOUR_CLOTH,
            new PrimaryAttributes(0, 0, 5)
        );

        Weapon testAxe = new Weapon
            (
                "Common axe",
                1,
                Slot.SLOT_WEAPON,
                WeaponType.WEAPON_AXE,
                new WeaponAttributes(7, 1.1)
            );

        Weapon testBow = new Weapon
            (
                "Common bow",
                1,
                Slot.SLOT_WEAPON,
                WeaponType.WEAPON_BOW,
                new WeaponAttributes(12, 0.8)
            );
        #endregion

        #region Test equip invalid Items throws exception
        [Fact]
        public void EquipWeapon_EquipHigherLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Weapon testAxeLevel2 = new Weapon
            (
                "Common axe",
                2,
                Slot.SLOT_WEAPON,
                WeaponType.WEAPON_AXE,
                new WeaponAttributes(7, 1.1)
            );
            // Act and Assert
            Assert.Throws<InvalidWeaponException>(() => this.warrior.EquipWeapon(testAxeLevel2));
        }

        [Fact]
        public void EquipArmour_EquipHigherLevelArmour_ShouldThrowInvalidArmourException()
        {
            // Arrange
            Armour testPlateBodyLevel2 = new Armour
            (
                "Common plate body armor",
                2,
                Slot.SLOT_BODY,
                ArmourType.ARMOUR_PLATE,
                new PrimaryAttributes(1, 0, 0)
            );
            // Act and Assert
            Assert.Throws<InvalidArmourException>(() => this.warrior.EquipArmour(testPlateBodyLevel2));
        }

        [Fact]
        public void EquipWeapon_EquipWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            // Arrange, act and assert
            Assert.Throws<InvalidWeaponException>(() => this.warrior.EquipWeapon(this.testBow));

        }

        [Fact]
        public void EquipArmour_EquipWrongArmourType_ShouldThrowInvalidArmourException()
        {
            // arrange, act and assert
            Assert.Throws<InvalidArmourException>(() => this.warrior.EquipArmour(this.testClothHead));
        }

        #endregion

        #region Test equip valid weapons return success
        [Fact]
        public void EquipWeapon_EquipValidWeaponType_ShouldReturnSuccessMessage()
        {
            // arrange
            string expected = "New weapon equipped!";
            // act
            string actual = this.warrior.EquipWeapon(this.testAxe);
            // assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmour_EquipValidArmourType_ShouldReturnSuccessMessage()
        {
            // arrange
            string expected = "New armour equipped!";
            // act
            string actual = this.warrior.EquipArmour(this.testPlateBody);
            // assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region Test damage calculations
        [Fact]
        public void TotalDamage_CalculateDamageWithNoWeapon_ShouldReturnCorrectDamage()
        {
            // arrange
            Warrior warriorSara = new Warrior("Sara");
            double expected = 1.0 * (1.0 + 5.0/100.0);
            // act
            double actual = warriorSara.TotalDamage();
            // assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalDamage_CalculateDamageWithWeapon_ShouldReturnDamageTimesDPS()
        {
            // arrange
            Warrior warriorSofie = new Warrior("Sofie");
            warriorSofie.EquipWeapon(this.testAxe);
            double expected = (7.0*1.1) * (1.0 + 5.0/100.0);
            // act
            double actual = warriorSofie.TotalDamage();
            // assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalDamage_CalculateDamageWithWeaponAndArmour_ShouldReturnDamageWithUpdatedAttributeTimesDPS()
        {
            // arrange
            Warrior warriorNina = new Warrior("Nina");
            warriorNina.EquipWeapon(this.testAxe);
            warriorNina.EquipArmour(this.testPlateBody);
            double expected = (7.0 * 1.1) * (1.0 + (5.0 + 1.0)/ 100.0);
            // act
            double actual = warriorNina.TotalDamage();
            // assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalDamage_CalculateDamageWithWeaponAndLevelUp_ShouldReturnDamageWithUpdatedAttributeTimesDPS()
        {
            // arrange
            Warrior warriorLisa = new Warrior("Lisa");
            warriorLisa.EquipWeapon(this.testAxe);
            warriorLisa.LevelUp();
            double expected = (7.0 * 1.1) * (1.0 + (5.0 + 3.0)/ 100.0);
            // act
            double actual = warriorLisa.TotalDamage();
            // assert
            Assert.Equal(expected, actual);
        }


        #endregion
    }
}
